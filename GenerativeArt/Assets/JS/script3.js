function pageLoaded(){
    
    let canvas = document.getElementById("canvas");
    let context = canvas.getContext("2d"); //2d context for canvas

    const ctx = document.getElementById('canvas').getContext('2d'); 

    let button = document.getElementById("button");
    button.addEventListener ("click", playSound);

    let newSound = new Audio("Assets/Audio/funny-spring-jump-140378.mp3");
    console.log(newSound);

    let step = 0;
    
    setInterval(playSound, 2000);
 
    function playSound(){
        console.log("calling sound loop");
        if (step < 4
          )
        {
            newSound.play();
            step++;
        };
    }

    ctx.fillStyle="black";
    //ctx.translate(250,0);
    ctx.fillRect(0, 0, 190, 250);
    ctx.fillRect(195, 0, 190, 250);
    ctx.fillRect(390, 0, 190, 250);
    ctx.fillRect(585, 0, 190, 250);
    ctx.fillRect(780, 0, 190, 250);
    ctx.fillRect(975, 0, 190, 250);

    for (let loop=1; loop <= 28; loop++){
      ctx.fillStyle = "#964B00";
      ctx.fillRect(0, (loop*15), 190, (loop*2));
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.fillStyle = "darkGoldenrod";
      ctx.fillRect(195, (loop*15), 190, (loop*2));
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.fillStyle = "goldenrod";
      ctx.fillRect(390, (loop*15), 190, (loop*2));
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.fillStyle = "orange";
      ctx.fillRect(585, (loop*15), 190, (loop*2));
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.fillStyle = "yellow";
      ctx.fillRect(780, (loop*15), 190, (loop*2));
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.fillStyle = "lightYellow";
      ctx.fillRect(975, (loop*15), 190, (loop*2));
    }
}

pageLoaded();
console.log("end of script");