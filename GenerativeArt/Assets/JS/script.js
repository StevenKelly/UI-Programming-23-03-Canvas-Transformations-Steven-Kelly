function pageLoaded(){
    
    let canvas = document.getElementById("canvas");
    let context = canvas.getContext("2d"); //2d context for canvas

    const ctx = document.getElementById('canvas').getContext('2d'); 

    let button = document.getElementById("button");
    button.addEventListener ("click", playSound);

    let newSound = new Audio("Assets/Audio/funny-spring-jump-140378.mp3");
    console.log(newSound);

    let step = 0;
    
    setInterval(playSound, 2000);
 
    function playSound(){
        console.log("calling sound loop");
        if (step < 4
          )
        {
            newSound.play();
            step++;
        };
    }



    for (let loop=1; loop <= 28; loop++){
      ctx.beginPath();
      ctx.strokeStyle = "red";
      ctx.arc(150,0,80,0,2*Math.PI);
      ctx.rotate(loop * Math.PI / 360);
      ctx.stroke();
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.beginPath();
      ctx.strokeStyle = "cyan";
      ctx.arc(80,20,80,0,2*Math.PI);
      ctx.rotate(loop * Math.PI / 360);
      ctx.stroke();
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.beginPath();
      ctx.strokeStyle = "white";
      ctx.arc(150,20,80,0,2*Math.PI);
      ctx.rotate(loop * Math.PI / 360);
      ctx.stroke();
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.beginPath();
      ctx.strokeStyle = "black";
      ctx.arc(150,100,80,0,2*Math.PI);
      ctx.rotate(loop * Math.PI / 360);
      ctx.stroke();
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.beginPath();
      ctx.strokeStyle = "gold";
      ctx.arc(100,28,700  ,0,2*Math.PI);
      ctx.rotate(loop * Math.PI / 360);
      ctx.stroke();
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.beginPath();
      ctx.moveTo(-10,7);
      ctx.lineTo(0,600);
      ctx.lineWidth=1;
      ctx.strokeStyle= "deepPink";
      ctx.rotate(loop * Math.PI /180);
      ctx.stroke();
    }

    for (let loop=1; loop <= 28; loop++){
      ctx.beginPath();
      ctx.moveTo(-490,40);
      ctx.lineTo(0,600);
      ctx.lineWidth=2;
      ctx.strokeStyle= "limeGreen";
      ctx.rotate(loop * Math.PI /180);
      ctx.stroke();
    }


    for (let loop=1; loop <= 28; loop++){
      ctx.beginPath();
      ctx.moveTo(-10,7);
      ctx.lineTo(0,600);
      ctx.lineWidth=1;
      ctx.strokeStyle= "lightBlue";
      ctx.rotate(loop * Math.PI /180);
      ctx.stroke();
    }

        ctx.translate(70,250);
for (let loop=1; loop <= 28; loop++){
  ctx.strokeStyle = "orange";
  ctx.strokeRect(200, (loop*15), 190, (loop*2));
  ctx.rotate(loop * Math.PI / 180);
}


}

pageLoaded();
console.log("end of script");